drop database if exists hrm;
create database hrm;
use hrm;


CREATE TABLE `user_details` (
  `id` varchar(255) NOT NULL,
  `address_line1` varchar(255) DEFAULT NULL,
  `adress_line2` varchar(255) DEFAULT NULL,
  `blood_group` varchar(255) DEFAULT NULL,
  `experince` varchar(255) DEFAULT NULL,
  `gender` varchar(255) DEFAULT NULL,
  `maritial_status` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

 CREATE TABLE `role` (
  `id` varchar(255) NOT NULL,
  `description` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `mapping` longtext,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ;


 CREATE TABLE `user` (
  `id` varchar(255) NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `createdby` varchar(255) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `updated_by` varchar(255) DEFAULT NULL,
  `designation` varchar(255) DEFAULT NULL,
  `date_of_birth` date DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `first_name` varchar(255) DEFAULT NULL,
  `joining_date` date DEFAULT NULL,
  `last_name` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `phone_number` varchar(255) DEFAULT NULL,
  `reporting_manager` varchar(255) DEFAULT NULL,
  `user_type` varchar(255) DEFAULT NULL,
  `role_id` varchar(255) DEFAULT NULL,
  `user_details_id` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UK_jukf6dprubncvcv1jrr9pceqy` (`user_details_id`),
  KEY `FKn82ha3ccdebhokx3a8fgdqeyy` (`role_id`),
  CONSTRAINT `FK3wsl4duq3n5imh005r68f3uar` FOREIGN KEY (`user_details_id`) REFERENCES `user_details` (`id`),
  CONSTRAINT `FKn82ha3ccdebhokx3a8fgdqeyy` FOREIGN KEY (`role_id`) REFERENCES `role` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1  ;




CREATE TABLE `department` (
  `id` varchar(255) NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `createdby` varchar(255) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `updated_by` varchar(255) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `head` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ;



CREATE TABLE `user_department` (
  `users_id` varchar(255) NOT NULL,
  `departments_id` varchar(255) NOT NULL,
  KEY `FK3ooyycnd5c56c30a8r9yulqea` (`departments_id`),
  KEY `FKdqllaer80w6bphtb7arsgubaa` (`users_id`),
  CONSTRAINT `FK3ooyycnd5c56c30a8r9yulqea` FOREIGN KEY (`departments_id`) REFERENCES `department` (`id`),
  CONSTRAINT `FKdqllaer80w6bphtb7arsgubaa` FOREIGN KEY (`users_id`) REFERENCES `user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;






INSERT INTO department(id, created_at, createdby, updated_at, updated_by, description, name, head)
VALUES ('73a351d356ba4502ab9a3de9bff01716',now(),'SYSTEM',now(),'SYSTEM','Default Department','DEFAULT' ,'Anonymous');

INSERT INTO user_details(id, address_line1, adress_line2, blood_group, experince, gender, maritial_status)
VALUES('11acac5bcf3745caa9371a80f1ddcdd1','kalka ji','New Delhi','O+','2 years','MALE','UnMarried');

INSERT INTO role(id, description, name, mapping)
values ('dbb85d66554c41a3a19e3168e8025b59','Admin with all privileges','SYSTEM_ADMIN','[{"resource":"ALL","permission":["ALL"]}]');

INSERT INTO role(id, description, name)
values ('2f2cfb1970f04eebaf12017dd387ae5e','Group privileges','GROUP_ADMIN');


INSERT INTO role(id, description, name)
values ('6132b50953494d5480a69b398673e88d','User Privileges','USER');

INSERT INTO user(id, created_at, createdby, updated_at, updated_by, designation, date_of_birth, email, first_name, joining_date, last_name, password, phone_number, reporting_manager, user_type, role_id, user_details_id)
VALUES('59b7c2441d23434e9f4a3aaad5c678c7',now(),'SYSTEM',now(),'SYSTEM','CEO',"1996-08-01",'saurav@gmail.com','Saurav',"2019-01-21",'Chhabra','$2a$10$IqZOiElijJ681Li5B8tUXOEFgCAUyil8J2ceQm4emUGzu7Fcfec/W','8700366276','Aman','Intern','dbb85d66554c41a3a19e3168e8025b59','11acac5bcf3745caa9371a80f1ddcdd1');

INSERT INTO user_department(users_id, departments_id)
VALUES('59b7c2441d23434e9f4a3aaad5c678c7','73a351d356ba4502ab9a3de9bff01716');





