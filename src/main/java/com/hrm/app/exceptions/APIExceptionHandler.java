package com.hrm.app.exceptions;

import com.hrm.app.response.ResponseDTO;
import com.hrm.app.utils.system.ResponseUtil;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.http.HttpStatus;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

@ControllerAdvice
@RestController
public class APIExceptionHandler {


    private final Log logger = LogFactory.getLog(getClass());

    @Autowired
    private ResponseUtil responseUtil;

    @Autowired
    private MessageSource messageSource;


    @ResponseStatus(value = HttpStatus.INTERNAL_SERVER_ERROR)
    @ExceptionHandler(value = {ApiExceptions.class})
    public ResponseDTO handleGenericException(ApiExceptions e) {
        logger.error(String.format("IotBackendApi: Got [[%s]] exception with message: %s", e.getClass().getName(), e.getMessage()));
        return responseUtil.exception(e.getCode());
    }

    @ResponseStatus(value = HttpStatus.BAD_REQUEST)
    @ExceptionHandler(value = {ValidationException.class})
    public ResponseDTO handleGenericException(ValidationException e) {
        logger.error(String.format("IotBackendApi: Got [[%s]] exception with message: %s", e.getClass().getName(), e.getMessage()));
        return responseUtil.validationFailed(e.getCode(), e.getMessage());
    }

    @ResponseStatus(value = HttpStatus.BAD_REQUEST)
    @ExceptionHandler(value = {MethodArgumentNotValidException.class})
    public ResponseDTO exception(MethodArgumentNotValidException e) {
        // ResponseDTO responseDTO = new ResponseDTO();
        List<FieldError> fieldErrors = e.getBindingResult().getFieldErrors();
        ArrayList<HashMap<String, String>> messageList = new ArrayList<>();
        for (FieldError s : fieldErrors) {
            HashMap<String, String> msgMap = new HashMap<>();
            msgMap.put("field", s.getField());
            msgMap.put("message", s.getDefaultMessage());
            messageList.add(msgMap);

        }
        return responseUtil.validationFailed(HttpStatus.NOT_FOUND.value(), "Error Message", messageList);
    }
}

