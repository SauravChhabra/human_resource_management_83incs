package com.hrm.app.exceptions;

import com.hrm.app.abstraction.ResponseCode;

public class ApiExceptions extends RuntimeException {


    private static final long serialVersionUID = 4919796816056888724L;
    private int code;
    private String message;

    public ApiExceptions(ResponseCode responseCode) {
        super(responseCode.getMessage());
        this.code = responseCode.getCode();
        this.message = responseCode.getMessage();
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    @Override
    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
