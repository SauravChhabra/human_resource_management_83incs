package com.hrm.app.exceptions;

import com.hrm.app.abstraction.ResponseCode;
import org.springframework.security.authentication.AuthenticationServiceException;


public class AuthMethodNotSupportedException extends AuthenticationServiceException {

    private static final long serialVersionUID = 1930383126809166641L;

    private int code;
    private String message;

    public AuthMethodNotSupportedException(ResponseCode responseCode) {
        super(responseCode.getMessage());
        this.code = responseCode.getCode();
        this.message = responseCode.getMessage();
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    @Override
    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
