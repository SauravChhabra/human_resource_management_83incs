package com.hrm.app.context;

import com.hrm.app.security.Privilege.UserContext;

import java.util.Objects;


public final class ExecutionContext {

    private final UserContext userContext;
    public static final ThreadLocal<ExecutionContext> CONTEXT = new ThreadLocal<>();

    public static void clear() {
        CONTEXT.remove();
    }

    public static ExecutionContext get() {
        return CONTEXT.get();
    }

    public static void set(ExecutionContext ec) {
        if (Objects.isNull(ec)) {
            throw new RuntimeException("Invalid Execution Context");
        }
        CONTEXT.set(ec);
    }

    public ExecutionContext(UserContext userContext) {
        this.userContext = userContext;
    }

    public UserContext getUserContext() {
        return userContext;
    }
}
