package com.hrm.app.security.Privilege;


import java.io.Serializable;
import java.util.List;

public class Entitlement implements Serializable {

    private static final long serialVersionUID = 4812438483811743293L;


    private String resource;
    private List<String> permissions;

    public Entitlement() {
    }

    public String getResource() {
        return this.resource;
    }

    public void setResource(String resource) {
        this.resource = resource;
    }

    public List<String> getPermissions() {
        return this.permissions;
    }

    public void setPermissions(List<String> permissions) {
        this.permissions = permissions;
    }

    @Override
    public String toString() {
        return "Entitlement{" +
                "resource='" + resource + '\'' +
                ", permissions=" + permissions +
                '}';
    }
}
