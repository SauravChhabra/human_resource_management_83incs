package com.hrm.app.security.Privilege;

import com.hrm.app.abstraction.UserType;
import org.apache.commons.lang3.StringUtils;

import java.util.List;

public class UserContext {

    private final String id;
    private final String username;
    private final String email;
    private final String phoneNumber;
    private final List<String> department;
    private final String roleId;
    private final String type;
    private final String userDetailsId;
    private final String reportingManager;
    private final String designation;



    private UserContext(String id, String username, String email, String phoneNumber,List<String> department,String roleId,String type,String userDetailsId,String reportingManager,String designation) {
        this.username = username;
        this.id = id;
        this.email = email;
        this.phoneNumber = phoneNumber;
        this.department = department;
        this.roleId=roleId;
        this.type=type;
        this.userDetailsId=userDetailsId;
        this.reportingManager=reportingManager;
        this.designation=designation;
    }

    public static UserContext create(String id, String username, String email, String phoneNumber,List<String> department,String roleId,String type,String userDetailsId,String reportingManager,String designation) {
        if (StringUtils.isBlank(username)) {
            throw new IllegalArgumentException("Username is blank: " + username);
        } else {
            return new UserContext(id, username, email, phoneNumber,department,roleId,type,userDetailsId,reportingManager,designation);
        }
    }

    public String getId() {
        return id;
    }

    public String getUsername() {
        return username;
    }

    public String getEmail() {
        return email;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public List<String> getDepartment() {
        return department;
    }

    public String getRoleId() {
        return roleId;
    }

    public String getType() {
        return type;
    }

    public String getUserDetailsId() {
        return userDetailsId;
    }

    public String getReportingManager() {
        return reportingManager;
    }

    public String getDesignation() {
        return designation;
    }
}
