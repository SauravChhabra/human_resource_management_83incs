package com.hrm.app.security.exceptions;

import com.hrm.app.security.tokenFactory.JwtToken;
import org.springframework.security.core.AuthenticationException;

/**
 * Created by Shubham Gupta on Fri, 22/2/19.
 */
public class JwtExpiredTokenException extends AuthenticationException {

    private static final long serialVersionUID = 5236081538470253104L;

    private JwtToken token;

    public JwtExpiredTokenException(String msg) {
        super(msg);
    }

    public JwtExpiredTokenException(JwtToken token, String msg, Throwable t) {
        super(msg, t);
        this.token = token;
    }

    public String token() {
        return this.token.getToken();
    }
}
