package com.hrm.app.security.exceptions;

import org.springframework.security.core.AuthenticationException;

/**
 * Created by Shubham Gupta on Fri, 22/2/19.
 */
public final class InvalidJwtToken extends AuthenticationException {

    private static final long serialVersionUID = 4875222617277710320L;

    public InvalidJwtToken(String msg, Throwable t) {
        super(msg, t);
    }

    public InvalidJwtToken(String msg) {
        super(msg);
    }
}
