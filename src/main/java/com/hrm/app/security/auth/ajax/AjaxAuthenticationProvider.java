package com.hrm.app.security.auth.ajax;

import com.hrm.app.security.Privilege.UserContext;
import com.hrm.app.entity.User;
import com.hrm.app.repository.UserRepository;
import com.hrm.app.utils.system.CommonUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Component;
import org.springframework.util.Assert;

@Component
public class AjaxAuthenticationProvider implements AuthenticationProvider {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private BCryptPasswordEncoder encoder;

    @Override
    public Authentication authenticate(Authentication authentication) throws AuthenticationException {
        Assert.notNull(authentication, "No Authentication Data Provided");

        String username = (String) authentication.getPrincipal();
        String password = (String) authentication.getCredentials();

        User user = null;
        try {
            user = userRepository.findUserByEmail(username);
        } catch (Exception e) {
            throw new BadCredentialsException("Authentication Failed. Username Not Valid");
        }
        if (user == null) {
            throw new UsernameNotFoundException("User not found : " + username);
        }

        if (!encoder.matches(password, user.getPassword())) {
            throw new BadCredentialsException("Authentication Failed.  Password not valid.");
        }

        UserContext userContext = UserContext.create(user.getId(), user.getFirstName() + " " + user.getLastName(),
                user.getEmail(), user.getPhoneNumber(),CommonUtils.generateScopeDepartmentPayloadForToken(user),user.getRole().getId(),user.getType().toString(),user.getUserDetails().getId(),user.getReportingManger(),user.getDesignation().toString());

        return new UsernamePasswordAuthenticationToken(userContext, null);
    }

    @Override
    public boolean supports(Class<?> authentication) {
        return (UsernamePasswordAuthenticationToken.class.isAssignableFrom(authentication));
    }
}
