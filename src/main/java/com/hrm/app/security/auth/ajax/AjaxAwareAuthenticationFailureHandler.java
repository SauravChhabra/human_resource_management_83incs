package com.hrm.app.security.auth.ajax;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.hrm.app.exceptions.AuthMethodNotSupportedException;
import com.hrm.app.security.exceptions.JwtExpiredTokenException;
import com.hrm.app.utils.system.ResponseUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.AuthenticationFailureHandler;
import org.springframework.stereotype.Component;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Objects;


@Component
public class AjaxAwareAuthenticationFailureHandler implements AuthenticationFailureHandler {

    private final ObjectMapper mapper;

    @Autowired
    private ResponseUtil responseUtil;

    @Autowired
    public AjaxAwareAuthenticationFailureHandler(ObjectMapper mapper) {
        this.mapper = mapper;
    }

    @Override
    public void onAuthenticationFailure(HttpServletRequest request, HttpServletResponse response, AuthenticationException e)
            throws IOException, ServletException {
        response.setStatus(HttpStatus.UNAUTHORIZED.value());
        response.setContentType(MediaType.APPLICATION_JSON_VALUE);

        if (e instanceof BadCredentialsException) {
           // if (Objects.nonNull(e.getMessage()) && e.getMessage().startsWith("Authentication Failed"))
                mapper.writeValue(response.getWriter(), this.responseUtil.exception(HttpStatus.UNAUTHORIZED.value(), e.getMessage()));
         //   else
        //        mapper.writeValue(response.getWriter(), this.responseUtil.exception(HttpStatus.UNAUTHORIZED.value(), "Invalid Token"));

        } else if (e instanceof JwtExpiredTokenException) {
            mapper.writeValue(response.getWriter(), this.responseUtil.exception(HttpStatus.UNAUTHORIZED.value(), e.getMessage()));
        } else if (e instanceof AuthMethodNotSupportedException) {
            mapper.writeValue(response.getWriter(), this.responseUtil.exception(HttpStatus.UNAUTHORIZED.value(), e.getMessage()));
        } else {
            mapper.writeValue(response.getWriter(), this.responseUtil.exception(HttpStatus.UNAUTHORIZED.value(), "Authentication Failed username    "));
        }
    }
}
