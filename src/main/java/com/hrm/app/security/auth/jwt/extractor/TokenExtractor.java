package com.hrm.app.security.auth.jwt.extractor;

/**
 * Created by Shubham Gupta on Thu, 21/2/19.
 */
public interface TokenExtractor {

    String extract(String payload);
}
