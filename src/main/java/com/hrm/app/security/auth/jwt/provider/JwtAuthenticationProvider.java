package com.hrm.app.security.auth.jwt.provider;

import com.hrm.app.abstraction.UserType;
import com.hrm.app.config.JwtConfig;
import com.hrm.app.context.ExecutionContext;
import com.hrm.app.security.Privilege.UserContext;
import com.hrm.app.security.exceptions.InvalidJwtToken;
import com.hrm.app.security.tokenFactory.JwtAuthenticationToken;
import com.hrm.app.security.tokenFactory.RawAccessJwtToken;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jws;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.stereotype.Component;

import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Objects;

@Component
@SuppressWarnings("unchecked")
public class JwtAuthenticationProvider implements AuthenticationProvider {

    private static final Logger LOG = LoggerFactory.getLogger(JwtAuthenticationProvider.class);

    private final JwtConfig jwtConfig;

    @Autowired
    public JwtAuthenticationProvider(JwtConfig jwtConfig) {
        this.jwtConfig = jwtConfig;
    }

    @Override
    public Authentication authenticate(Authentication authentication) throws AuthenticationException {
        RawAccessJwtToken rawAccessToken = (RawAccessJwtToken) authentication.getCredentials();
        Jws<Claims> jwsClaims = null;
        UserContext context = null;
        try {
            jwsClaims = rawAccessToken.parseClaims(jwtConfig.getTokenSigningKey());
        } catch (Exception e) {
            LOG.error("###### INVALID OR COMPROMISED TOKEN ######");
        }
        if (Objects.nonNull(jwsClaims) && !isTokenExpired(jwsClaims.getBody().getExpiration())) {
            String subject = jwsClaims.getBody().getSubject();
            String id = jwsClaims.getBody().getId();
            String email = jwsClaims.getBody().get("email", String.class);
            String phoneNumber = jwsClaims.getBody().get("phoneNumber", String.class);
          List department=jwsClaims.getBody().get("departments",List.class);
          String roleId=jwsClaims.getBody().get("roleId",String.class);
          String type=jwsClaims.getBody().get("type",String.class);
          String userDetailId=jwsClaims.getBody().get("User Details",String.class);
          String reportingManager=jwsClaims.getBody().get("Reporting Manager",String.class);
          String designation=jwsClaims.getBody().get("Disignation",String.class);

            context = UserContext.create(id, subject, email, phoneNumber,department,roleId,type,userDetailId,reportingManager,designation);
            ExecutionContext ec = new ExecutionContext(context);
            ExecutionContext.CONTEXT.set(ec);
        } else {
            throw new InvalidJwtToken("Invalid Token");
        }
        return new JwtAuthenticationToken(context);
    }

    private boolean isTokenExpired(Date tokenExpiryDate) {
        if (tokenExpiryDate.after(Calendar.getInstance().getTime())) {
            return false;
        }
        return true;
    }

    @Override
    public boolean supports(Class<?> authentication) {
        return (JwtAuthenticationToken.class.isAssignableFrom(authentication));
    }
}
