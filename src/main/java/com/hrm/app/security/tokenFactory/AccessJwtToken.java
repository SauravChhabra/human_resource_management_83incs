package com.hrm.app.security.tokenFactory;

import com.fasterxml.jackson.annotation.JsonIgnore;
import io.jsonwebtoken.Claims;

/**
 * Created by Shubham Gupta on Wed, 20/2/19.
 */
public final class AccessJwtToken implements JwtToken {

    private final String rawToken;
    @JsonIgnore
    private Claims claims;

    protected AccessJwtToken(final String accessToken, Claims claims) {
        this.rawToken = accessToken;
        this.claims = claims;
    }

    @Override
    public String getToken() {
        return this.rawToken;
    }

    public Claims getClaims() {
        return claims;
    }
}
