package com.hrm.app.security.tokenFactory;

import com.hrm.app.config.JwtConfig;
import com.hrm.app.security.Privilege.UserContext;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Date;

@Component
public class JwtTokenFactory {

    private final JwtConfig settings;

    @Autowired
    public JwtTokenFactory(JwtConfig settings) {
        this.settings = settings;
    }

    public AccessJwtToken createAccessJwtToken(UserContext userContext) {
        if (StringUtils.isBlank(userContext.getUsername())) {
            throw new IllegalArgumentException("Cannot Persist JWT Token without username");
        }

        Claims claims = Jwts.claims().setSubject(userContext.getUsername());
        claims.setId(userContext.getId());
        claims.put("email", userContext.getEmail());
        claims.put("phoneNumber", userContext.getPhoneNumber());
        claims.put("department", userContext.getDepartment());
        claims.put("roleId",userContext.getRoleId());
        claims.put("type",userContext.getType());
        claims.put("User Details",userContext.getUserDetailsId());
        claims.put("Reporting Manager",userContext.getReportingManager());
        claims.put("Designation",userContext.getDesignation());
        LocalDateTime currentTime = LocalDateTime.now();

        String accessToken = Jwts.builder()
                .setClaims(claims)
                .setIssuer(settings.getTokenIssuer())
                .setIssuedAt(Date.from(currentTime.atZone(ZoneId.systemDefault()).toInstant()))
                .setExpiration(Date.from(currentTime
                        .plusMinutes(settings.getTokenExpirationTime())
                        .atZone(ZoneId.systemDefault()).toInstant()))
                .signWith(SignatureAlgorithm.HS512, settings.getTokenSigningKey())
                .compact();

        return new AccessJwtToken(accessToken, claims);
    }
}
