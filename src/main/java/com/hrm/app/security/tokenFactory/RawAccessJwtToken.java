package com.hrm.app.security.tokenFactory;

import com.hrm.app.security.exceptions.JwtExpiredTokenException;
import io.jsonwebtoken.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.authentication.BadCredentialsException;

/**
 * Created by Shubham Gupta on Fri, 22/2/19.
 */
public final class RawAccessJwtToken implements JwtToken {

    private static final Logger LOG = LoggerFactory.getLogger(RawAccessJwtToken.class);

    private String accessToken;

    public RawAccessJwtToken(String accessToken) {
        this.accessToken = accessToken;
    }

    /**
     * Parses and validates JWT Token signature.
     *
     * @throws BadCredentialsException
     * @throws JwtExpiredTokenException
     */
    public Jws<Claims> parseClaims(String signingKey) {
        try {
            return Jwts.parser().setSigningKey(signingKey).parseClaimsJws(this.accessToken);
        } catch (UnsupportedJwtException | MalformedJwtException | IllegalArgumentException ex) {
            LOG.error("Invalid JWT Token");
            throw new BadCredentialsException("Invalid JWT token: ", ex);
        } catch (ExpiredJwtException expiredEx) {
            LOG.error("JWT Token is expired");
            throw new JwtExpiredTokenException(this, "JWT Token expired", expiredEx);
        }
    }

    @Override
    public String getToken() {
        return accessToken;
    }
}
