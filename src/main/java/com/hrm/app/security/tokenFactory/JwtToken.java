package com.hrm.app.security.tokenFactory;

/**
 * Created by Shubham Gupta on Wed, 20/2/19.
 */
public interface JwtToken {

    String getToken();
}
