package com.hrm.app.controller;


import com.hrm.app.abstraction.ApiResponseCode;
import com.hrm.app.abstraction.DocumentType;
import com.hrm.app.request.UserRequest;
import com.hrm.app.response.ResponseDTO;
import com.hrm.app.service.UserService;
import com.hrm.app.utils.system.ResponseUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.Valid;

@RestController
@RequestMapping("/api/v1/users")
public class UserController {

    @Autowired
    private UserService userService;

    @Autowired
    private ResponseUtil responseUtil;


    /*@RequestMapping(method = RequestMethod.POST, value = "/login")
    public ResponseDTO loginUser(@RequestBody UserRequest userRequest) {

        return userService.loginUser(userRequest);
    }*/


    @RequestMapping(method = RequestMethod.POST)
    public ResponseDTO<?> adduser(@Valid @RequestBody UserRequest userRequest) {
        userService.addUser(userRequest);
        return responseUtil.ok(null, ApiResponseCode.SUCCESS);

    }

    @RequestMapping(method = RequestMethod.GET)
    public ResponseDTO<?> getUsers() {

        return responseUtil.ok(userService.getUsers(), ApiResponseCode.SUCCESS);
    }


    @RequestMapping(method = RequestMethod.GET, value = "/{id}")
    public ResponseDTO<?> getUserById(@PathVariable String id) {

        return responseUtil.ok(userService.getUserById(id), ApiResponseCode.SUCCESS);
    }

    @RequestMapping(method = RequestMethod.PUT, value = "/{id}")
    public ResponseDTO<?> updateUserById(@PathVariable String id, @RequestBody UserRequest userRequest) {
        userService.updateUserById(id, userRequest);

        return responseUtil.ok(null, ApiResponseCode.SUCCESS);
    }

    @RequestMapping(method = RequestMethod.DELETE, value = "/{id}")
    public ResponseDTO<?> deleteUserById(@PathVariable String id) {
        userService.deleteUserById(id);

        return responseUtil.ok(null, ApiResponseCode.SUCCESS);
    }
    @RequestMapping(method = RequestMethod.GET,value ="/{id}/projects")
    public ResponseDTO<?> getProjectsByUserId(@PathVariable String id){
        return responseUtil.ok(userService.getProjectsByUserId(id),ApiResponseCode.SUCCESS);
    }

    @RequestMapping(method = RequestMethod.GET,value = "/enum")
    public  ResponseDTO<?> getEducationtypes(){
        return responseUtil.ok(userService.getEducationtypes(),ApiResponseCode.SUCCESS);
    }

    @RequestMapping(method = RequestMethod.POST,value = "/docs/{id}")
    public ResponseDTO<?> uploadDocuments(@RequestParam MultipartFile file,@RequestParam DocumentType type,@PathVariable String id){
        userService.uploadDocuments(file,type,id);
        return responseUtil.ok(ApiResponseCode.SUCCESS);
    }

    @RequestMapping(method = RequestMethod.DELETE,value = "/docs/{id}")
    public ResponseDTO<?> deleteDocument(@PathVariable String id){
        userService.deleteDocument(id);
        return responseUtil.ok(ApiResponseCode.SUCCESS);
    }

    @RequestMapping(method = RequestMethod.GET,value = "/docs/{id}")
    public ResponseDTO<?> getDocuments(@RequestParam(required = false) DocumentType type,@PathVariable String id){

        return responseUtil.ok(userService.getDocuments(type,id),ApiResponseCode.SUCCESS);
    }

    @RequestMapping(method = RequestMethod.GET,value = "/docs")
    public  ResponseDTO<?> getDocumentTypes(){
        return responseUtil.ok(userService.getDocumentTypes(),ApiResponseCode.SUCCESS);
    }

    @RequestMapping(method = RequestMethod.PUT,value = "/docs/{id}/{uId}")
    public ResponseDTO<?> updateDocuments(@RequestParam MultipartFile file,@RequestParam DocumentType type,@PathVariable String id,@PathVariable String uId){
        userService.updateDocuments(file,type,id,uId);
        return responseUtil.ok(ApiResponseCode.SUCCESS);
    }

}
