package com.hrm.app.controller;


import com.hrm.app.abstraction.ApiResponseCode;
import com.hrm.app.request.ProjectRequest;
import com.hrm.app.response.ResponseDTO;
import com.hrm.app.service.ProjectService;
import com.hrm.app.utils.system.ResponseUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("api/v1/projects")
public class ProjectController {
    @Autowired
    private ProjectService projectService;

    @Autowired
    private ResponseUtil responseUtil;

    @RequestMapping(method = RequestMethod.POST)
    public ResponseDTO<?> addProject(@RequestBody ProjectRequest projectRequest) {
        projectService.addProject(projectRequest);
        return responseUtil.ok(null, ApiResponseCode.SUCCESS);
    }


    @RequestMapping(method = RequestMethod.GET)
    public ResponseDTO<?> getProjects() {
        return responseUtil.ok(projectService.getProjects(), ApiResponseCode.SUCCESS);
    }

    @RequestMapping(method = RequestMethod.GET, value = "/{id}")
    public ResponseDTO<?> getProjectById(@PathVariable String id) {
        return responseUtil.ok(projectService.getProjectById(id), ApiResponseCode.SUCCESS);

    }

    @RequestMapping(method = RequestMethod.PUT, value = "/{id}")
    public ResponseDTO<?> updateProjectById(@PathVariable String id, @RequestBody ProjectRequest projectRequest) {
        projectService.updateProjectById(id, projectRequest);
        return responseUtil.ok(null, ApiResponseCode.SUCCESS);
    }

    @RequestMapping(method = RequestMethod.DELETE, value = "/{id}")
    public ResponseDTO<?> deleteProjectById(@PathVariable String id) {
        projectService.deleteProjectById(id);
        return responseUtil.ok(null, ApiResponseCode.SUCCESS);
    }


}
