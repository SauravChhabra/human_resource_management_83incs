package com.hrm.app.controller;


import com.hrm.app.Resources.Role;
import com.hrm.app.abstraction.ApiResponseCode;
import com.hrm.app.annotation.PreHandle;
import com.hrm.app.request.RoleRequest;
import com.hrm.app.response.ResponseDTO;
import com.hrm.app.service.RoleService;
import com.hrm.app.utils.system.ResponseUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("api/v1/roles")
public class RoleController {

    @Autowired
    private RoleService roleService;

    @Autowired
    private ResponseUtil responseUtil;

    @RequestMapping(method = RequestMethod.POST)
    @PreHandle(requestMethhod = RequestMethod.POST,resourceType = Role.class)
    public ResponseDTO<?> addRole(@RequestBody RoleRequest roleRequest) throws Exception{
        roleService.addRole(roleRequest);
        return responseUtil.ok(null, ApiResponseCode.SUCCESS);

    }

    @RequestMapping(method = RequestMethod.GET)
    public ResponseDTO<?> getAllRoles() {
        return responseUtil.ok(roleService.getAllRoles(), ApiResponseCode.SUCCESS);
    }

    @RequestMapping(method = RequestMethod.GET, value = "/{id}")
    public ResponseDTO<?> getRoleByID(@PathVariable String id) {
        return responseUtil.ok(roleService.getRoleByID(id), ApiResponseCode.SUCCESS);
    }

    @RequestMapping(method = RequestMethod.PUT, value = "/{id}")
    public ResponseDTO<?> updateRoleById(@PathVariable String id, @RequestBody RoleRequest roleRequest) throws Exception{
        roleService.updateRoleById(id, roleRequest);
        return responseUtil.ok(null, ApiResponseCode.SUCCESS);
    }

      @RequestMapping(method = RequestMethod.DELETE,value = "/{id}")
    public ResponseDTO<?> deleteRoleById(@PathVariable String id){
        roleService.deleteRoleById(id);
        return responseUtil.ok(null,ApiResponseCode.SUCCESS);
      }
}
