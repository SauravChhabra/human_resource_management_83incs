package com.hrm.app.controller;


import com.hrm.app.abstraction.ApiResponseCode;
import com.hrm.app.request.DepartmentRequest;
import com.hrm.app.response.ResponseDTO;
import com.hrm.app.service.DepartmentService;
import com.hrm.app.utils.system.ResponseUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("api/v1/departments")
public class DepartmentController {

    @Autowired
    private DepartmentService departmentService;

    @Autowired
    private ResponseUtil responseUtil;

    @RequestMapping(method = RequestMethod.POST)
    public ResponseDTO<?> addDepartment(@RequestBody DepartmentRequest departmentRequest) {
        departmentService.addDepartment(departmentRequest);
        return responseUtil.ok(null, ApiResponseCode.SUCCESS);
    }

    @RequestMapping(method = RequestMethod.GET)
    public ResponseDTO<?> getAllDepartments() {

        return responseUtil.ok(departmentService.getAllDepartments(), ApiResponseCode.SUCCESS);
    }

    @RequestMapping(method = RequestMethod.GET, value = "/{id}")
    public ResponseDTO<?> getDepartmentById(@PathVariable String id) {

        return responseUtil.ok(departmentService.getDepartmentById(id), ApiResponseCode.SUCCESS);
    }

    @RequestMapping(method = RequestMethod.PUT, value = "/{id}")
    public ResponseDTO<?> updateDepartment(@PathVariable String id, @RequestBody DepartmentRequest departmentRequest) {
        departmentService.updateDepartment(id, departmentRequest);
        return responseUtil.ok(null, ApiResponseCode.SUCCESS);
    }

    @RequestMapping(method = RequestMethod.DELETE, value = "/{id}")
    public ResponseDTO<?> deleteDepartmentById(@PathVariable String id) {
        departmentService.deleteDepartmentById(id);
        return responseUtil.ok(null, ApiResponseCode.SUCCESS);
    }

    @RequestMapping(method = RequestMethod.GET, value = "/{id}/users")
    public ResponseDTO<?> getUsersByDepartmentId(@PathVariable String id) {
        return responseUtil.ok(departmentService.getUsersByDepartmentId(id), ApiResponseCode.SUCCESS);
    }
}
