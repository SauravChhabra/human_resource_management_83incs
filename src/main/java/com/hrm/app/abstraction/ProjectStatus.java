package com.hrm.app.abstraction;

public enum ProjectStatus {

    Open,
    Closed,
    InProgress,
    ReOpened
}
