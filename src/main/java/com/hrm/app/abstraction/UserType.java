package com.hrm.app.abstraction;

public enum UserType {

    Permanent,
    Temporary,
    Intern,
    Freelancer;


};

