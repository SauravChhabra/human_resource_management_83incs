package com.hrm.app.abstraction;

/**
 * Created by Shubham Gupta on Fri, 22/2/19.
 */
public interface ResponseCode {

    int getCode();

    String getMessage();


}
