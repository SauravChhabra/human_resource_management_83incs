package com.hrm.app.abstraction;

public enum ApiResponseCode implements ResponseCode {

    SUCCESS(0, "SUCCESS"),
    ERROR(1, "ERROR"),
    BAD_REQUEST(2, "BAD_REQUEST"),
    RESOURCE_PERMISSION_DENIED(14, "RESOURCE_PERMISSION_DENIED"),
    RESOURCE_NOT_ALLOWED(15, "RESOURCE_NOT_ALLOWED");

    private int code;
    private String message;

    ApiResponseCode(int code, String message) {
        this.code = code;
        this.message = message;
    }

    @Override
    public int getCode() {
        return code;
    }

    @Override
    public String getMessage() {
        return message;
    }
}
