package com.hrm.app.abstraction;

public enum Designation {

    CEO,
    DIRECTOR,
    PROJECT_MANAGER,
    TEAM_LEAD,
    SENIOR_SOFTWARE_ENGINEER,
    SOFTWARE_ENGINEER,
    INTERN,
    ASISTANT_MANAGER,
    ACCOUNTANT,
    CLERK

};
