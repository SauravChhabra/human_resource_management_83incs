package com.hrm.app.abstraction;

public enum DocumentType {

    ADHAAR_CARD,
    VOTER_ID_CARD,
    PAN_CARD,
    USER_IMAGE,
    METRIC_MARKSHEET,
    INTERMEDIATE_MARKSHEET


}
