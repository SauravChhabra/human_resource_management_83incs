package com.hrm.app.entity;


import com.fasterxml.jackson.annotation.JsonIgnore;
import com.hrm.app.security.Privilege.Entitlement;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

@Entity
@Table(name = "role")
public class Role implements Serializable {


    private static final long serialVersionUID = -1042037539043544162L;

    @Id
    @Column(name = "id")
    private String id;

    @Column(name = "name")
    private String name;

    @Column(name = "description")
    private String description;

    @Lob
    @JsonIgnore
    private String mapping;

    @Transient
    private List<Entitlement> roleEntitlement;

    public List<Entitlement> getRoleEntitlement() {
        return roleEntitlement;
    }

    public void setRoleEntitlement(List<Entitlement> roleEntitlement) {
        this.roleEntitlement = roleEntitlement;
    }

    public String getMapping() {
        return mapping;
    }

    public void setMapping(String mapping) {
        this.mapping = mapping;
    }


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
