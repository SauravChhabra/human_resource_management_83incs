package com.hrm.app.entity;


import com.hrm.app.abstraction.DocumentType;

import javax.persistence.*;
import java.io.Serializable;

@Table(name ="documents")
@Entity
public class Document implements Serializable {

    private static final long serialVersionUID = -3341276075731305256L;


    @Id
    @Column
    private String id;

    @Column
    @Enumerated(EnumType.STRING)
    private DocumentType type;

    @Column
    private String path;

    @Column
    private String url;

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public DocumentType getType() {
        return type;
    }

    public void setType(DocumentType type) {
        this.type = type;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }
}
