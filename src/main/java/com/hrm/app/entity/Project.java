package com.hrm.app.entity;


import com.hrm.app.abstraction.ProjectStatus;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.List;

@Entity
@Table(name = "project")
public class Project implements Serializable {

    private static final long serialVersionUID = -5874429557842477417L;

    @Id
    @Column
    private String id;

    @Column
    private String name;

    @Column
    private String clientName;

    @Column
    @Enumerated(EnumType.STRING)
    private ProjectStatus projectStatus;

    @Temporal(TemporalType.DATE)
    @Column
    private Date startingDate;

    @Temporal(TemporalType.DATE)
    @Column
    private Date submissionDate;

    @Column
    private String head;

    @Column
    private String details;


    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(name = "project_user")
    private List<User> users;

    public List<User> getUsers() {
        return users;
    }

    public void setUsers(List<User> users) {
        this.users = users;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getClientName() {
        return clientName;
    }

    public void setClientName(String clientName) {
        this.clientName = clientName;
    }

    public ProjectStatus getProjectStatus() {
        return projectStatus;
    }

    public void setProjectStatus(ProjectStatus projectStatus) {
        this.projectStatus = projectStatus;
    }

    public Date getStartingDate() {
        return startingDate;
    }

    public void setStartingDate(Date startingDate) {
        this.startingDate = startingDate;
    }

    public Date getSubmissionDate() {
        return submissionDate;
    }

    public void setSubmissionDate(Date submissionDate) {
        this.submissionDate = submissionDate;
    }

    public String getHead() {
        return head;
    }

    public void setHead(String head) {
        this.head = head;
    }

    public String getDetails() {
        return details;
    }

    public void setDetails(String details) {
        this.details = details;
    }
}
