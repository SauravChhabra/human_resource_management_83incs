package com.hrm.app.entity;


import com.hrm.app.abstraction.EduactionType;

import javax.persistence.*;
import java.io.Serializable;

@Table
@Entity
public class Education implements Serializable {


    private static final long serialVersionUID = 8879654895191103558L;

    @Id
    private String id;
    @Column
    @Enumerated(EnumType.STRING)
    private EduactionType type;
    @Column
    private Integer startingYear;
    @Column
    private Integer passingYear;
    @Column
    private String institute;
    @Column
    private String courseName;

    public EduactionType getType() {
        return type;
    }

    public void setType(EduactionType type) {
        this.type = type;
    }

    public Integer getStartingYear() {
        return startingYear;
    }

    public void setStartingYear(Integer startingYear) {
        this.startingYear = startingYear;
    }

    public Integer getPassingYear() {
        return passingYear;
    }

    public void setPassingYear(Integer passingYear) {
        this.passingYear = passingYear;
    }

    public String getInstitute() {
        return institute;
    }

    public void setInstitute(String institute) {
        this.institute = institute;
    }

    public String getCourseName() {
        return courseName;
    }

    public void setCourseName(String courseName) {
        this.courseName = courseName;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }


}
