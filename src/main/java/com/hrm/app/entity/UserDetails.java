package com.hrm.app.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.hrm.app.abstraction.GenderType;

import javax.persistence.*;
import java.io.Serializable;


@Entity
@Table(name = "user_details")
public class UserDetails implements Serializable {


    private static final long serialVersionUID = -732366362681446908L;
    @Id
    @Column(name = "id")
    private String id;


    @Column(name = "address_line1")
    private String addressLine1;
    @Column(name = "adress_Line2")
    private String addressLine2;
    @Column(name = "maritial_status")
    private String martialStatus;
    @Column(name = "gender")
    @Enumerated(EnumType.STRING)
    private GenderType gender;
    @Column(name = "blood_group")
    private String bloodGroup;
    @Column(name = "experince")
    private String experince;

    @JsonIgnore
    @OneToOne(mappedBy = "userDetails")
    private User user;

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }


    public String getMartialStatus() {
        return martialStatus;
    }

    public void setMartialStatus(String martialStatus) {
        this.martialStatus = martialStatus;
    }

    public GenderType getGender() {
        return gender;
    }

    public void setGender(GenderType gender) {
        this.gender = gender;
    }

    public String getBloodGroup() {
        return bloodGroup;
    }

    public void setBloodGroup(String bloodGroup) {
        this.bloodGroup = bloodGroup;
    }



    public String getExperince() {
        return experince;
    }

    public void setExperince(String experince) {
        this.experince = experince;
    }

    public String getAddressLine1() {
        return addressLine1;
    }

    public void setAddressLine1(String addressLine1) {
        this.addressLine1 = addressLine1;
    }

    public String getAddressLine2() {
        return addressLine2;
    }

    public void setAddressLine2(String addressLine2) {
        this.addressLine2 = addressLine2;
    }


}
