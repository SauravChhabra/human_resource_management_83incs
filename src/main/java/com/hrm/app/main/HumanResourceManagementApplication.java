package com.hrm.app.main;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

@SpringBootApplication(scanBasePackages = "com.hrm.app")
@EntityScan("com.hrm.app.entity")
@EnableJpaRepositories(basePackages = "com.hrm.app.repository")
@EnableWebMvc
public class HumanResourceManagementApplication {

    public static void main(String[] args) {
        SpringApplication.run(HumanResourceManagementApplication.class, args);
    }

}
