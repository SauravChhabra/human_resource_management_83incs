package com.hrm.app.config;


import com.hrm.app.security.Privilege.UserContext;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.domain.AuditorAware;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;
import org.springframework.security.core.context.SecurityContextHolder;

import java.util.Optional;

@Configuration
@EnableJpaAuditing
//(auditorAwareRef = "auditorProvider")
public class JpaAuditingConfig implements AuditorAware<String> {
    @Override
    public Optional<String> getCurrentAuditor() {

        UserContext context = (UserContext) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        return Optional.of(context.getId());
    }



   /* @Bean
    public AuditorAware<String> auditorProvider() {

        *//*
          if you are using spring security, you can get the currently logged username with following code segment.
          SecurityContextHolder.getContext().getAuthentication().getName()
         *//*
        return () -> Optional.ofNullable("Saurav");
    }*/
}
