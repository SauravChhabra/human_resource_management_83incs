package com.hrm.app.response;


@SuppressWarnings("rawtypes")
public interface ResponseDTO<T> {


    int getCode();

    void setCode(int code);

    String getMessage();

    ResponseDTO setMessage(String message);

    T getData();

    void setData(T data);
}
