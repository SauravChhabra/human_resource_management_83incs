package com.hrm.app.response;

import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.hrm.app.abstraction.ResponseCode;
import org.springframework.stereotype.Component;

import java.util.Locale;

@Component
@JsonPropertyOrder({"code", "data", "message"})
@SuppressWarnings({"rawtypes", "unused"})
public class ApiResponseDTO<T> implements ResponseDTO<T> {

    private int code;
    private T data;
    private String message = "";
    private Object list;

    public ApiResponseDTO() {
    }

    public ApiResponseDTO(int code, T data, String message, Object list) {
        this.code = code;
        this.data = data;
        this.message = message;
        this.list = list;
    }

    public ApiResponseDTO(int code, String message, T data) {
        this.code = code;
        this.data = data;
        this.message = message;
    }

    public ApiResponseDTO(String message, T data) {
        this.data = data;
        this.message = message;
    }

    public ApiResponseDTO(ResponseCode responseCode, T data) {
        this.code = responseCode.getCode();
        this.message = responseCode.getMessage();
        this.data = data;
    }


    private Locale getLocale(String locale) {
        return locale != null ? new Locale(locale) : Locale.UK;
    }

    @Override
    public String toString() {
        return "ApiResponseDTO{" +
                "code=" + code +
                ", data=" + data +
                ", message='" + message + '\'' +
                ", list=" + list +
                '}';
    }

    @Override
    public int getCode() {
        return code;
    }

    @Override
    public void setCode(int code) {
        this.code = code;
    }

    @Override
    public String getMessage() {
        return message;
    }

    @Override
    public ResponseDTO setMessage(String message) {
        this.message = message;
        return this;
    }

    @Override
    public T getData() {
        return data;
    }

    @Override
    public void setData(T data) {
        this.data = data;
    }
}
