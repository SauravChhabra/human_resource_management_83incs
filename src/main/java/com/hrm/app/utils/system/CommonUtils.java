package com.hrm.app.utils.system;

import com.hrm.app.context.ExecutionContext;
import com.hrm.app.entity.User;
import com.hrm.app.security.Privilege.UserContext;
import netscape.security.Privilege;

import javax.servlet.http.HttpServletRequest;
import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public final class CommonUtils {
    public static String generateUUID() {

        return UUID.randomUUID().toString().replaceAll("-", "");
    }

    public static String getLocaleStringFromClientHTTPRequest(HttpServletRequest httpServletRequest) {
        return httpServletRequest.getLocale().getLanguage();
    }

    public static List<String> generateScopeDepartmentPayloadForToken(User user) {
        List<String> departmentList = new ArrayList<>();
        user.getDepartments().forEach(department -> {
            departmentList.add(department.getId());
        });

        return departmentList;
    }

    public static String getUserIdOfLoggedInUser() {
        return ExecutionContext.get().getUserContext().getId();
    }

}
