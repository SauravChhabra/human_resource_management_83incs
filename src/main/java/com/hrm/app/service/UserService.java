package com.hrm.app.service;

import com.hrm.app.abstraction.DocumentType;
import com.hrm.app.abstraction.EduactionType;
import com.hrm.app.constants.ApplicationConstants;
import com.hrm.app.entity.*;
import com.hrm.app.exceptions.ValidationException;
import com.hrm.app.repository.DepartmentRepository;
import com.hrm.app.repository.DocumentRepository;
import com.hrm.app.repository.RoleRepository;
import com.hrm.app.repository.UserRepository;
import com.hrm.app.request.UserRequest;
import com.hrm.app.utils.system.CommonUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.util.*;

import static com.hrm.app.constants.ApplicationConstants.DOT;
import static com.hrm.app.constants.ApplicationConstants.UNDER_SCORE_STRING;

@Service
public class UserService {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private DepartmentRepository departmentRepository;

    @Autowired
    private BCryptPasswordEncoder encoder;

    @Autowired
    private RoleRepository roleRepository;

    @Autowired
    private S3Service s3Service;

    @Autowired
    private DocumentRepository documentRepository;


    public void addUser(UserRequest userRequest) {

        List<String> departmentsid = userRequest.getDepartmentsId();
        User user = userRepository.findUserByEmail(userRequest.getEmail());
        if (Objects.nonNull(user)) {
            throw new ValidationException(HttpStatus.BAD_REQUEST.value(), "Email is already taken");

        }

        List<Department> departmentList = new ArrayList<>();
        for (String id : departmentsid) {
            Department department = departmentRepository.findById(id).get();
            departmentList.add(department);
        }


        User user1 = new User();
        user1.setId(CommonUtils.generateUUID());
        user1.setFirstName(userRequest.getFirstName());
        user1.setLastName(userRequest.getLastName());
        user1.setEmail(userRequest.getEmail());
        user1.setPassword(encoder.encode(userRequest.getPassword()));
        user1.setPhoneNumber(userRequest.getPhoneNumber());
        if (departmentList.isEmpty()) {
            throw new ValidationException(HttpStatus.BAD_REQUEST.value(), "Please provide atleast one Department id");
        }
        user1.setDepartments(departmentList);
        user1.setDesignation(userRequest.getDesignation());
        user1.setType(userRequest.getType());
        user1.setReportingManger(userRequest.getReportingManger());
        user1.setJoiningDate(userRequest.getJoiningDate());
        user1.setDob(userRequest.getDob());
        Role role = roleRepository.findById(userRequest.getRoleId()).orElse(new Role());
        if (Objects.isNull(role.getId())) {
            throw new ValidationException(HttpStatus.BAD_REQUEST.value(), "Please provide valid role.");
        }
        user1.setRole(role);
        //----------------------------------------------------------------------------------------//
        UserDetails userDetails = new UserDetails();
        userDetails.setId(CommonUtils.generateUUID());
        userDetails.setAddressLine1(userRequest.getAddressLine1());
        userDetails.setAddressLine2(userRequest.getAddressLine2());
        userDetails.setBloodGroup(userRequest.getBloodGroup());
        userDetails.setGender(userRequest.getGender());
        userDetails.setMartialStatus(userRequest.getMartialStatus());
        userDetails.setExperince(userRequest.getExperince());
        user1.setUserDetails(userDetails);
        //------------------------------------------------------------------------------------------//
//        user1.setEducationList(new ArrayList<Education>());
        for (Education education : userRequest.getEducationList()) {

            education.setId(CommonUtils.generateUUID());
//            Education edu = new Education();
            // edu.setId(education.getId());
           /* edu.setId(CommonUtils.generateUUID());
            edu.setCourseName(education.getCourseName());
            edu.setInstitute(education.getInstitute());
            edu.setPassingYear(education.getPassingYear());
            edu.setStartingYear(education.getStartingYear());
            edu.setType(education.getType());*/


            user1.getEducationList().add(education);
        }

        userRepository.save(user1);
    }

    public List<User> getUsers() {
        return userRepository.findAll();


    }


    public User getUserById(String id) {

        User user = userRepository.findById(id).orElse(new User());
        if (Objects.nonNull(user.getId())) {
            return user;
        } else {
            throw new ValidationException(HttpStatus.BAD_REQUEST.value(), "please provide valid user ID ");

        }


    }

    public void updateUserById(String id, UserRequest userRequest) {
        List<String> departmentsid = userRequest.getDepartmentsId();
        User user = userRepository.findById(id).orElse(new User());
        if (Objects.isNull(user.getId())) {
            throw new ValidationException(HttpStatus.BAD_REQUEST.value(), "please provide valid user ID for updation of record");

        } else {
            User user1 = user;
            if (Objects.nonNull(userRequest.getFirstName()))
                user1.setFirstName(userRequest.getFirstName());

            if (Objects.nonNull(userRequest.getLastName()))
                user1.setLastName(userRequest.getLastName());

            if (Objects.nonNull(userRequest.getPhoneNumber()))
                user1.setPhoneNumber(userRequest.getPhoneNumber());

            if (Objects.nonNull(userRequest.getPassword()))
                user1.setPassword(encoder.encode(userRequest.getPassword()));

            if (Objects.nonNull(userRequest.getDesignation()))
                user1.setDesignation(userRequest.getDesignation());

            if (Objects.nonNull(userRequest.getType()))
                user1.setType(userRequest.getType());

            if (Objects.nonNull(userRequest.getReportingManger()))
                user1.setReportingManger(userRequest.getReportingManger());

            if (Objects.nonNull(userRequest.getJoiningDate()))
                user1.setJoiningDate(userRequest.getJoiningDate());

            if (Objects.nonNull(userRequest.getDob()))
                user1.setDob(userRequest.getDob());

            User user2 = userRepository.findUserByEmailAndIdNotIn(userRequest.getEmail(), id);
            if (Objects.nonNull(user2)) {
                throw new ValidationException(HttpStatus.BAD_REQUEST.value(), "Email is already taken");

            } else {
                user1.setEmail(userRequest.getEmail());
            }
            List<Department> departmentList = new ArrayList<>();
            for (String deptId : departmentsid) {
                Department department = departmentRepository.findById(deptId).get();
                departmentList.add(department);
            }
            if (departmentList.isEmpty()) {
                throw new ValidationException(HttpStatus.BAD_REQUEST.value(), "Please provide atleast one Department id");
            }
            user1.setDepartments(departmentList);


            UserDetails userDetails = user.getUserDetails();
            userDetails.setId(user.getUserDetails().getId());
            if (Objects.nonNull(userRequest.getAddressLine1()))
                userDetails.setAddressLine1(userRequest.getAddressLine1());


            if (Objects.nonNull(userRequest.getAddressLine2()))
                userDetails.setAddressLine2(userRequest.getAddressLine2());

            if (Objects.nonNull(userRequest.getBloodGroup()))
                userDetails.setBloodGroup(userRequest.getBloodGroup());

            if (Objects.nonNull(userRequest.getGender()))
                userDetails.setGender(userRequest.getGender());

            if (Objects.nonNull(userRequest.getMartialStatus()))
                userDetails.setMartialStatus(userRequest.getMartialStatus());

            if (Objects.nonNull(userRequest.getExperince()))
                userDetails.setExperince(userRequest.getExperince());

            user1.setUserDetails(userDetails);

            userRepository.save(user1);

        }

    }

    public void deleteUserById(String id) {


        User user = userRepository.findById(id).orElse(new User());
        if (Objects.isNull(user.getId())) {
            throw new ValidationException(HttpStatus.BAD_REQUEST.value(), "please provide valid user ID for Deletion of record");
        } else {

            userRepository.deleteProjectUser(id);
            userRepository.deleteById(id);
        }
    }

    public List<Project> getProjectsByUserId(String id) {
        User user = userRepository.findById(id).orElse(new User());
        if (Objects.nonNull(user.getId())) {
            return user.getProjects();
        } else {
            throw new ValidationException(HttpStatus.BAD_REQUEST.value(), "please provide valid user id");
        }
    }

    public EnumSet<EduactionType> getEducationtypes() {
        EnumSet<EduactionType> eduactionTypes = EnumSet.allOf(EduactionType.class);
        return eduactionTypes;
    }

    public void uploadDocuments(MultipartFile file, DocumentType type, String userId) {
        User user = new User();

        if (Objects.nonNull(userId)) {
            user = userRepository.findById(userId).orElse(new User());
        }


        if (Objects.isNull(file) || file.isEmpty())
            throw new ValidationException(HttpStatus.BAD_REQUEST.value(), "File cannot be empty");

        int fileLength = file.getOriginalFilename().length();
        if (file.getOriginalFilename().indexOf(ApplicationConstants.DOT) > -1) {
            fileLength = file.getOriginalFilename().indexOf(ApplicationConstants.DOT);

        }
        String extension = file.getOriginalFilename().split("\\.")[1];

        String humanReadableFileName = file.getOriginalFilename().substring(0, fileLength) + UNDER_SCORE_STRING + Calendar.getInstance().getTimeInMillis() + DOT + extension;

        String fileNameAndPath = user.getEmail() + "/" + type + "/" + humanReadableFileName;
        HashMap<String, String> uploadData = s3Service.uploadFile(file, fileNameAndPath);
        if (Objects.isNull(uploadData) || Objects.isNull(uploadData.get("secure_url")) || uploadData.get("secure_url").isEmpty()) {
            throw new ValidationException(HttpStatus.BAD_REQUEST.value(), "Error in upload file");
        }
        Document document = new Document();
        document.setId(CommonUtils.generateUUID());
        document.setType(type);
        document.setPath(fileNameAndPath);
        document.setUrl(uploadData.get("secure_url"));
        user.getDocumentList().add(document);


        userRepository.save(user);


    }


    public void updateDocuments(MultipartFile file, DocumentType type, String docId, String userId) {


        User user = userRepository.findById(userId).get();

        if (Objects.isNull(user.getId())) {
            throw new ValidationException(HttpStatus.BAD_REQUEST.value(), "please provide valid User Id");

        }

        Document document = documentRepository.findById(docId).get();
        if (Objects.isNull(document.getId())) {
            throw new ValidationException(HttpStatus.BAD_REQUEST.value(), "please provide valid document Id");
        }

        String uID = documentRepository.getUserFromDocumentId(docId);
        if (!uID.equals(userId)) {
            throw new ValidationException(HttpStatus.BAD_REQUEST.value(), "Document does not belong to the provided user Id");
        }

        if (Objects.isNull(file) || file.isEmpty()) {
            throw new ValidationException(HttpStatus.BAD_REQUEST.value(), "file canbot be empty");
        }


        int fileLength = file.getOriginalFilename().length();
        if (file.getOriginalFilename().indexOf(ApplicationConstants.DOT) > -1) {
            fileLength = file.getOriginalFilename().indexOf(ApplicationConstants.DOT);

        }
        String extension = file.getOriginalFilename().split("\\.")[1];
       /* if (Objects.isNull(extension) || extension.isEmpty()) {
            String humanReadableFileName = file.getOriginalFilename().substring(0, fileLength) + UNDER_SCORE_STRING + Calendar.getInstance().getTimeInMillis();
        } else {*/


            String humanReadableFileName = file.getOriginalFilename().substring(0, fileLength) + UNDER_SCORE_STRING + Calendar.getInstance().getTimeInMillis() + DOT + extension;

        String fileNameAndPath = user.getEmail() + "/" + type + "/" + humanReadableFileName;
        HashMap<String, String> uploadData = s3Service.uploadFile(file, fileNameAndPath);
        if (Objects.isNull(uploadData) || Objects.isNull(uploadData.get("secure_url")) || uploadData.get("secure_url").isEmpty()) {
            throw new ValidationException(HttpStatus.BAD_REQUEST.value(), "Error in upload file");
        }
        s3Service.deleteFile(document.getPath());


        Document document1 = document;
        document1.setType(type);
        document1.setUrl(uploadData.get("secure_url"));
        document1.setPath(fileNameAndPath);
        user.getDocumentList().add(document1);
        userRepository.save(user);


    }

    public void deleteDocument(String docId) {

        Document document = documentRepository.findById(docId).get();

        if (Objects.isNull(document.getId())) {
            throw new ValidationException(HttpStatus.BAD_REQUEST.value(), "please provide valid document Id");
        } else {
            documentRepository.deleteById(docId);
        }
        String path = document.getPath();

        s3Service.deleteFile(path);

    }


    public Object getDocuments(DocumentType type, String userId) {
        User user = userRepository.findById(userId).orElse(new User());

        if (Objects.isNull(user.getId())) {
            throw new ValidationException(HttpStatus.BAD_REQUEST.value(), "please provide valid user ID");

        }
        if (Objects.nonNull(type)) {
            return user.getDocumentList().stream().filter(q -> q.getType().equals(type)).findAny().orElse(new Document());
        }
        return user.getDocumentList();
    }

    public EnumSet<DocumentType> getDocumentTypes() {
        EnumSet<DocumentType> types = EnumSet.allOf(DocumentType.class);
        return types;
    }

}
