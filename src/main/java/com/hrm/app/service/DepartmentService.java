package com.hrm.app.service;


import com.hrm.app.entity.Department;
import com.hrm.app.entity.User;
import com.hrm.app.exceptions.ValidationException;
import com.hrm.app.repository.DepartmentRepository;
import com.hrm.app.request.DepartmentRequest;
import com.hrm.app.utils.system.CommonUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Objects;

@Service
public class DepartmentService {


    @Autowired
    private DepartmentRepository departmentRepository;


    public void addDepartment(DepartmentRequest departmentRequest) {

        Department department = departmentRepository.findDepartmentByName(departmentRequest.getName());
        if (Objects.nonNull(department)) {
            throw new ValidationException(HttpStatus.BAD_REQUEST.value(), "Department already exsist");

        }
        Department department1 = new Department();
        department1.setId(CommonUtils.generateUUID());
        department1.setName(departmentRequest.getName());
        department1.setDescription(departmentRequest.getDescription());
        department1.setHead(departmentRequest.getHead());
        departmentRepository.save(department1);


    }

    public List<Department> getAllDepartments() {


        return departmentRepository.findAll();

    }

    public Department getDepartmentById(String id) {

        Department department = departmentRepository.findById(id).orElse(new Department());
        if (Objects.nonNull(department.getId())) {
            return department;
        } else {
            throw new ValidationException(HttpStatus.BAD_REQUEST.value(), "please provide valid Department ID ");


        }
    }

    public void updateDepartment(String id, DepartmentRequest departmentRequest) {

        Department department = departmentRepository.findById(id).orElse(new Department());
        if (Objects.isNull(department.getId())) {
            throw new ValidationException(HttpStatus.BAD_REQUEST.value(), "please provide valid Department ID for updation of record");


        } else {
            Department department1 = department;

            department1.setId(department.getId());

            // Department department2=departmentRepository.findDepartmentByName(departmentRequest.getName());
            Department department2 = departmentRepository.findDepartmentByNameAndIdNotIn(departmentRequest.getName(), id);

            if (Objects.nonNull(department2)) {
                throw new ValidationException(HttpStatus.BAD_REQUEST.value(), "Department already exsist");

            } else {
                department1.setName(departmentRequest.getName());
            }
            if (Objects.nonNull(departmentRequest.getDescription()))
                department1.setDescription(departmentRequest.getDescription());

            if (Objects.nonNull(departmentRequest.getHead()))
                department1.setHead(departmentRequest.getHead());

            departmentRepository.save(department1);
        }


    }


    public void deleteDepartmentById(String id) {
        Department department = departmentRepository.findById(id).orElse(new Department());
        if (Objects.nonNull(department.getId())) {

            departmentRepository.deleteDepartment(id);
            departmentRepository.deleteById(id);
        } else {
            throw new ValidationException(HttpStatus.BAD_REQUEST.value(), "please provide valid Department id for deletion");

        }
    }

    public List<User> getUsersByDepartmentId(String id) {

        //  List<User> users = new ArrayList<>();

        Department department = departmentRepository.findById(id).orElse(new Department());
        if (Objects.nonNull(department.getId())) {
            //  users = department.getUsers();
            return department.getUsers();


        } else {
            throw new ValidationException(HttpStatus.BAD_REQUEST.value(), "No users are available in this departemnt");


        }
    }
}
