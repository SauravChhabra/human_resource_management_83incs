package com.hrm.app.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.hrm.app.Resources.Common;
import com.hrm.app.entity.Role;
import com.hrm.app.exceptions.ValidationException;
import com.hrm.app.repository.RoleRepository;
import com.hrm.app.request.RoleRequest;
import com.hrm.app.security.Privilege.Entitlement;
import com.hrm.app.security.Privilege.RoleEntitlement;
import com.hrm.app.utils.system.CommonUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@Service
public class RoleService {


    @Autowired
    private RoleRepository roleRepository;

    public void addRole(RoleRequest roleRequest) throws Exception {


        Role role = roleRepository.findRoleByName(roleRequest.getName());
        if (Objects.nonNull(role)) {
            throw new ValidationException(HttpStatus.BAD_REQUEST.value(), "Role already exsist");

        } else {
            Role role1 = new Role();
            role1.setId(CommonUtils.generateUUID());
            role1.setName(roleRequest.getName());
            role1.setDescription(roleRequest.getDescription());


            List<String> permissions = new ArrayList<>();
            permissions.add("ALL");

            RoleEntitlement roleEntitlement = new RoleEntitlement();
            roleEntitlement.setResource(Common.class.getSimpleName());
            roleEntitlement.setPermissions(permissions);

            List<RoleEntitlement> roleEntitlements = new ArrayList<>();
            roleEntitlements.add(roleEntitlement);

            role1.setMapping(createRoleEntitlementMapping(roleEntitlements));
            roleRepository.save(role1);
        }
    }

    private String createRoleEntitlementMapping(List<RoleEntitlement> roleEntitlements) throws Exception {
        List<Entitlement> entitlements = new ArrayList<>();
        if (roleEntitlements != null) {
            roleEntitlements.forEach(roleEntitlement -> {
                entitlements.add(roleEntitlement.mapToEntitlement());
            });
        }
        return new ObjectMapper().writeValueAsString(entitlements);
    }


    public List<Role> getAllRoles() {


        return roleRepository.findAll();

    }

    public Role getRoleByID(String id) {
        Role role = roleRepository.findById(id).get();
        if (Objects.nonNull(role.getId())) {
            return role;
        } else {
            throw new ValidationException(HttpStatus.BAD_REQUEST.value(), "Povide valid Role id");
        }
    }


    public void updateRoleById(String id, RoleRequest roleRequest) throws Exception {
        boolean flag=false;
        Role role = roleRepository.findById(id).get();
        if (Objects.isNull(role.getId())) {
            throw new ValidationException(HttpStatus.BAD_REQUEST.value(), "Provide valid role ID for updation of record");
        } else {
            Role updateRole = role;

            if (Objects.nonNull(roleRequest.getName())) {
                Role checkRoleName = roleRepository.findRoleByNameAndIdNotIn(id, roleRequest.getName());
                if (Objects.nonNull(checkRoleName)) {
                    throw new ValidationException(HttpStatus.BAD_REQUEST.value(), "Admin already exsist");
                } else {
                    updateRole.setName(roleRequest.getName());
                }
            }
            if (Objects.nonNull(roleRequest.getDescription()))
                updateRole.setDescription(roleRequest.getDescription());

            if (roleRequest.getRoleEntitlement() != null && !roleRequest.getRoleEntitlement().isEmpty()) {
                String roleMapping = role.getMapping();
                String requestRoleMapping = createRoleEntitlementMapping(roleRequest.getRoleEntitlement());
                if (!requestRoleMapping.equals(roleMapping)) {
                    if (Objects.nonNull(requestRoleMapping) && !requestRoleMapping.isEmpty()) {
                        if (!requestRoleMapping.equals(roleMapping)) {
                            flag = true;
                        }
                    } else {
                        flag = true;
                    }
                    role.setMapping(createRoleEntitlementMapping(roleRequest.getRoleEntitlement()));
                }
            }

            roleRepository.save(updateRole);
        }
    }

    public void deleteRoleById(String id) {
        Role role = roleRepository.findById(id).orElse(new Role());
        if (Objects.nonNull(role.getId())) {
            roleRepository.deleteById(id);
        } else {
            throw new ValidationException(HttpStatus.BAD_REQUEST.value(), "please provide valid Id for deletion of record");
        }
    }
}
