package com.hrm.app.service;

import com.hrm.app.entity.Project;
import com.hrm.app.entity.User;
import com.hrm.app.exceptions.ValidationException;
import com.hrm.app.repository.ProjectRepository;
import com.hrm.app.repository.UserRepository;
import com.hrm.app.request.ProjectRequest;
import com.hrm.app.utils.system.CommonUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;


@Service
public class ProjectService {

    @Autowired
    private ProjectRepository projectRepository;

    @Autowired
    private UserRepository userRepository;


    public void addProject(ProjectRequest projectRequest) {
        List<String> userIds = projectRequest.getUserids();
        Project project = projectRepository.findProjectByName(projectRequest.getName());
        if (Objects.nonNull(project)) {
            throw new ValidationException(HttpStatus.BAD_REQUEST.value(), "Project name is already taken");
        }


        List<User> userList = new ArrayList<>();
        for (String id : userIds) {
            User user = userRepository.findById(id).get();
            userList.add(user);

        }

        Project project1 = new Project();
        project1.setId(CommonUtils.generateUUID());
        project1.setName(projectRequest.getName());
        project1.setClientName(projectRequest.getClientName());
        project1.setHead(projectRequest.getHead());
        project1.setProjectStatus(projectRequest.getProjectStatus());
        project1.setDetails(projectRequest.getDetails());
        project1.setStartingDate(projectRequest.getStartingDate());
        project1.setSubmissionDate(projectRequest.getSubmissionDate());
        project1.setUsers(userList);
        projectRepository.save(project1);

    }

    public List<Project> getProjects() {

        return projectRepository.findAll();
    }

    public Project getProjectById(String id) {
        Project project = projectRepository.findById(id).orElse(new Project());
        if (Objects.nonNull(project.getId())) {
            return project;
        } else
            throw new ValidationException(HttpStatus.BAD_REQUEST.value(), "please provide valid user ID ");

    }

    public void updateProjectById(String id, ProjectRequest projectRequest) {
        List<String> userIds = projectRequest.getUserids();
        Project project = projectRepository.findById(id).orElse(new Project());
        if (Objects.isNull(project.getId())) {
            throw new ValidationException(HttpStatus.BAD_REQUEST.value(), "provide valid id for updation of record");
        } else {
            Project project1 = project;
            if (Objects.nonNull(projectRequest.getName())) {
                Project project2 = projectRepository.findProjectByNameAndIdNotIn(projectRequest.getName(), id);
                if (Objects.nonNull(project2)) {
                    throw new ValidationException(HttpStatus.BAD_REQUEST.value(), "Project Name is alrready taken");
                } else {

                    project1.setName(projectRequest.getName());
                }
            }


            if (Objects.nonNull(projectRequest.getClientName()))
                project1.setClientName(projectRequest.getClientName());

            if (Objects.nonNull(projectRequest.getDetails()))
                project1.setDetails(projectRequest.getDetails());

            if (Objects.nonNull(projectRequest.getProjectStatus()))
                project1.setProjectStatus(projectRequest.getProjectStatus());

            if (Objects.nonNull(projectRequest.getHead()))
                project1.setHead(projectRequest.getHead());

            if (Objects.nonNull(projectRequest.getSubmissionDate()))
                project1.setSubmissionDate(projectRequest.getSubmissionDate());

            if (Objects.nonNull(projectRequest.getStartingDate()))
                project1.setStartingDate(projectRequest.getStartingDate());


            List<User> userList = new ArrayList<>();
            for (String uId : userIds) {
                User user = userRepository.findById(uId).get();
                userList.add(user);

            }
            project1.setUsers(userList);

            projectRepository.save(project1);
        }

    }

    public void deleteProjectById(String id) {
        Project project = projectRepository.findById(id).orElse(new Project());
        if (Objects.nonNull(project.getId())) {
            projectRepository.deleteById(id);
        } else {
            throw new ValidationException(HttpStatus.BAD_REQUEST.value(), "provide valid id for deletion of record");

        }
    }
}
