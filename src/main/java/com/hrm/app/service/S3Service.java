package com.hrm.app.service;

import com.amazonaws.auth.*;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import com.amazonaws.services.s3.model.CannedAccessControlList;
import com.amazonaws.services.s3.model.ObjectMetadata;
import com.amazonaws.services.s3.model.PutObjectRequest;
import com.hrm.app.abstraction.ApiResponseCode;
import com.hrm.app.config.S3Config;
import com.hrm.app.exceptions.ApiExceptions;
import com.hrm.app.exceptions.ValidationException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.PostConstruct;
import java.io.File;
import java.io.FileInputStream;
import java.util.HashMap;
import java.util.Objects;


@Service
public class S3Service {
    @Autowired
    S3Config s3Config;

    private static final Logger LOG = LogManager.getLogger("org");

    @PostConstruct
    public AmazonS3 getS3Client(){
        AmazonS3 s3Client = null;
        try{
            s3Client = AmazonS3ClientBuilder.standard()
                    .withCredentials(new AWSStaticCredentialsProvider(new BasicAWSCredentials(s3Config.getAccessKey(), s3Config.getSecretKey())))
                    .withRegion(s3Config.getRegion())
                    .build();
        }catch (Exception e){
            LOG.error("Unable to establish connection with AWS S3.");
        }

        return s3Client;
    }

    public HashMap<String, String> uploadFile(MultipartFile file, String pathAndName) {
        if (!Objects.isNull(file) && !file.isEmpty()) {
            String bucket = this.s3Config.getBucket();
            if (!Objects.isNull(bucket) && !bucket.isEmpty()) {
                AmazonS3 s3Client = this.getS3Client();
                if (Objects.isNull(s3Client)) {
                    throw new ValidationException(HttpStatus.BAD_REQUEST.value(), "Error in Establishing connection with AWS S3.");
                } else {
                    HashMap result = new HashMap();

                    try {
                        ObjectMetadata metadata = new ObjectMetadata();
                        metadata.setContentLength(file.getSize());
                        PutObjectRequest por = (new PutObjectRequest(bucket, pathAndName, file.getInputStream(), metadata)).withCannedAcl(CannedAccessControlList.PublicRead);
                        s3Client.putObject(por);
                    } catch (Exception e) {
                        e.printStackTrace();
                        LOG.error("Error while upload.");
                        throw new ValidationException(HttpStatus.BAD_REQUEST.value(), "Error while upload.");
                    }

                    result.put("secure_url", s3Client.getUrl(bucket, pathAndName).toExternalForm());
                    return result;
                }
            } else {
                throw new ValidationException(HttpStatus.BAD_REQUEST.value(), "No bucket found in property");
            }
        } else {
            throw new ValidationException(HttpStatus.BAD_REQUEST.value(), "File cannot be empty");
        }
    }


    public void deleteFile(String pathAndName) {
        if (!Objects.isNull(pathAndName) && !pathAndName.isEmpty()) {
            String bucket = this.s3Config.getBucket();
            if (!Objects.isNull(bucket) && !bucket.isEmpty()) {
                AmazonS3 s3Client = this.getS3Client();
                if (Objects.isNull(s3Client)) {
                    throw new ValidationException(HttpStatus.BAD_REQUEST.value(), "Error in Establishing connection with AWS S3.");
                } else {
                    try {
                        s3Client.deleteObject(bucket, pathAndName);
                    } catch (Exception var5) {
                        LOG.error("Error while deleting file from S3.");
                        throw new ValidationException(HttpStatus.BAD_REQUEST.value(), "Error while deleting file from S3.");
                    }
                }
            } else {
                throw new ValidationException(HttpStatus.BAD_REQUEST.value(), "No bucket found in property");
            }
        } else {
            throw new ValidationException(HttpStatus.BAD_REQUEST.value(), "File name cannot be blank.");
        }
    }
}
