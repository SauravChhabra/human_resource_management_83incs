package com.hrm.app.repository;


import com.hrm.app.entity.Document;
import com.hrm.app.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;

@Repository
public interface DocumentRepository extends JpaRepository<Document,String> {

    @Transactional

    @Query(value = "select document_list_id from documents where id=?1",nativeQuery = true)
    String getUserFromDocumentId(String docId);
}
