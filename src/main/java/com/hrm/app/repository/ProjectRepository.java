package com.hrm.app.repository;

import com.hrm.app.entity.Project;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;


@Repository
public interface   ProjectRepository extends JpaRepository<Project, String> {

    Project findProjectByName(String name);

    Project findProjectByNameAndIdNotIn(String name, String id);
}