package com.hrm.app.repository;


import com.hrm.app.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;

@Repository
public interface UserRepository extends JpaRepository<User, String> {

    User findUserByEmail(String email);

    User findUserByEmailAndIdNotIn(String email, String id);

    /*@Transactional
    @Modifying
    @Query(value = "delete from project_user where users_id=?1", nativeQuery = true)
    void deleteProjectUser(String userId);*/


    @Transactional
    @Modifying
    @Query(value = "delete from project_user where users_id= :userId", nativeQuery = true)
    void deleteProjectUser(@Param("userId") String userId);

}
