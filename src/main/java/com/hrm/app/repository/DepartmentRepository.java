package com.hrm.app.repository;


import com.hrm.app.entity.Department;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;

@Repository
public interface DepartmentRepository extends JpaRepository<Department, String> {

    Department findDepartmentByName(String name);

    Department findDepartmentByNameAndIdNotIn(String name, String id);

    @Transactional
    @Modifying
    @Query(value = "delete from  user_department where departments_id=?1",nativeQuery = true)
    void deleteDepartment(String deptId);


}
