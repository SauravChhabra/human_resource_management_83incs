package com.hrm.app.repository;

import com.hrm.app.entity.Role;
import org.springframework.data.jpa.repository.JpaRepository;

public interface RoleRepository extends JpaRepository<Role, String> {

    Role findRoleByName(String roleName);

    Role findRoleByNameAndIdNotIn(String id, String name);
}
