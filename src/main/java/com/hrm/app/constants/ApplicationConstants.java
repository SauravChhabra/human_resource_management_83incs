/**
 * @Author Jayant Puri
 * @Created 10-Apr-2017
 */
package com.hrm.app.constants;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;

/**
 * @author jayant
 */
public interface ApplicationConstants {

    DecimalFormat TWO_DECIMAL_PLACE = new DecimalFormat("#.##");
    DecimalFormat FOUR_DECIMAL_PLACE = new DecimalFormat("#.####");
    HashSet<String> ALLOWED_DAYS_OF_WEEK = new HashSet<>();
    int HOURLY_DATA = 60;
    int MINUTE_DATA = 15;
    int ZERO = 0;
    int DESC = -1;
    int ASC = 1;

    String GET = "get";
    String EMPTY_STRING = "";
    String HYPHEN_STRING = "-";
    String UNDER_SCORE_STRING = "_";
    String DOT = ".";
    String PERCENT_SYMBOL_20 = "%20";
    String PERCENT_SYMBOL_27 = "%27";
    String PERCENT_SYMBOL_28 = "%28";
    String PERCENT_SYMBOL_29 = "%29";
    String PERCENT_SYMBOL_21 = "%21";
    String PERCENT_SYMBOL_3C = "%3C";
    String PERCENT_SYMBOL_5D = "%5D";
    String PERCENT_SYMBOL_7D = "%7D";
    String PERCENT_SYMBOL_7B = "%7B";
    String PERCENT_SYMBOL_5B = "%5B";
    String PERCENT_SYMBOL_3E = "%3E";
    String PERCENT_SYMBOL_3F = "%3F";
    String SINGLE_QUOTE = "'";
    String EXCLAMATION = "!";
    String QUESTION_MARK = "?";
    String OPEN_BRACKET = "(";
    String CLOSE_BRACKET = ")";
    String CLOSE_BRACKET_CURLY = "}";
    String OPEN_BRACKET_CURLY = "{";
    String CLOSE_BRACKET_SQUARE = "]";
    String OPEN_BRACKET_SQUARE = "[";
    String LESS_THAN = "<";
    String GREATER_THAN = ">";
    String LOG_DASH_SEPARATOR = "-------";
    String COMMA = ",";
    String OAUTH_ERROR_KEY = "error_description";
    String DEFAULT_ERROR = "Some error occurred";

    int CALENDER_CRITERIA_TODAY = -1;
    int CALENDER_CRITERIA_YESTERDAY = -1;
    int CALENDER_CRITERIA_LAST_3_DAYS = -3;
    int CALENDER_CRITERIA_LAST_7_DAYS = -7;
    int ONE = 1;
    int CALENDER_CRITERIA_LAST_30_DAYS = -30;
    int CALENDER_CRITERIA_LAST_90_DAYS = -90;

    String TIMESTAMP = "timestamp";
    String DATE = "date";


    String INTERNAL_DNS = "internalDns";
    String MAIL_FROM = "mailFrom";
    String SPACE = " ";
    String APP_MAIL_CONFIGURED = "app.mail.configured";

    String GATEWAY = "GATEWAY";
    String EXTENDER = "EXTENDER";
    String MICROSERVICE_CDS = "actiontec/api/v2/cds";
    String MICROSERVICE_JOB = "/actiontec/api/unauth/job";

    String GOOGLE_CLIENT_ID = "867656699342-8lqnqivohjl381s7uf27la9gv3omgls9.apps.googleusercontent.com";
    String EXTERNAL_USER_PASSWORD = "iPEcZw5BdbpNRH-YmRIIM2iY";
    String TOKEN_DECODE_URL = "https://www.googleapis.com/oauth2/v3/tokeninfo";
    String GOOGLE_USER_ACCOUNT_INFO = "https://www.googleapis.com/oauth2/v1/userinfo";
    String VOICE_ACCESS_TOKEN = "CTJW26pGI3jwYObUO7ctpcoxaqBM0SAKoptR";
    String ALEXA = "alexa";
    String GOOGLE = "google";
    String DEFAULT = "DEFAULT-";
    String RPC_RESULT = "OK";
    String JOB_STARTED = "STARTED";
    String JOB_NOT_STARTED = "NOT_STARTED";
    String DEFAULT_COMPARTMENT = "DEFAULT";

    String END_USER = "USER";
    String SYSTEM_ADMIN = "SYSTEM_ADMIN";
    String ETL_TYPE_STREAM = "STREAM_QUERY_NAME";
    String ETL_TYPE_HISTORY = "HISTORY_QUERY_NAME";

    String BAND_5G = "5G";
    String BAND_24G = "24G";

    //CACHE MAPS CONSTANTS
    String NETWORK_INFO_KEY = "netInfo_key";
    String NETWORK_INFO_MAP_NAME = "NET_INFO";
    String KEY = "key";
    String MAP = "map";
    String COMPARTMENT = "COMPARTMENT";
    String ROLE = "ROLE";
    String NAVIGATION = "NAVIGATION";
    String SYSTEM_CONFIG = "SYSTEM_CONFIG";
    String TOKEN = "TOKEN";
    String ISP = "ISP";
    String ISP_KEY = "isp_key";
    String SYSTEM_CONFIG_KEY = "system_config";
    String COLLECTIONS_KEY = "actiontec_collections";
    String COLLECTIONS = "COLLECTIONS";
    String RGW_COUNT = "rgw_count";
    String STA_COUNT = "sta_count";

    Integer[] contentFilter = {0, 1, 2, 3, 4, 5, 6, 7, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31};
    ArrayList<Integer> contentFilterList = new ArrayList<>(Arrays.asList(contentFilter));

    String TELUS = "telus";

    String WIRE_SHARK_URL = "https://code.wireshark.org/review/gitweb?p=wireshark.git;a=blob_plain;f=manuf;hb=HEAD";

    String MAC_VENDOR_DETAIL = "VENDOR_DETAIL";
    String COLON = ":";
    String TIME_OUT = "TIME-OUT";

    Long THREAD_TO_SLEEP = 1000L;

    String ETL_RESTART_TOPIC = "CTRL";

    String WIFI_HEALTH = "WIFI_HEALTH";
    String LAST_DATA_UPLINK_RATE = "LAST_DATA_UPLINK_RATE";
    String LAST_DATA_DOWN_LINK_RATE = "LAST_DATA_DOWN_LINK_RATE";
    String KAFKA_TOPICS_KEY = "KAFKA_TOPICS";

    String MQTT_CONNECTION_NAME = "Default_Channel_for_Cluster";

    String DASHBOARD_COUNTS = "DashboardCounts";
    String GLOBAL = "GLOBAL";

    String COMPARTMENT_LIST_MAP_NAME = "COMPARTMENT_LIST";
    String COMPARTMENT_INFO = "COMPARTMENT_INFO";
    String WINDSTREAM = "windstream";

    // OAuth2.0 Config
    String MICROSOFT_TOKEN_ENDPOINT = "https://login.microsoftonline.com/__TENANT_ID__/oauth2/v2.0/token";
    String MICROSOFT_ME_ENDPOINT = "https://graph.microsoft.com/v1.0/me";
    String TENANT_ID = "__TENANT_ID__";
    String SCOPE = "https://graph.microsoft.com/user.read";
    String GRANT_TYPE = "authorization_code";
    String CLIENT_IDENTIFIER_SECRET_KEY = "AmQFtlXPHU7DYi1XLS8ElBmjuYwBtBdVF74fb777e093f647b9a0ec8d6ff3ff2bd6";
    String REPORT_TYPE_EXCEL = "EXCEL";

    String _FILE_TYPE = "csv";

    // Well Known Config Constants
    String AUTHORIZATION_INFO_URL = "authorization_endpoint";
    String TOKEN_INFO_URL = "token_endpoint";
    String USER_INFO_URL = "userinfo_endpoint";

    // Well Known Config User Claims

    String SUB = "sub";
    String GIVEN_NAME = "given_name";
    String FAMILY_NAME = "family_name";
    String ACCESS_TOKEN = "access_token";
    String ADMIN_USER_ID = "bdc124f4da284a3396ec283ccc068fcb";

}
