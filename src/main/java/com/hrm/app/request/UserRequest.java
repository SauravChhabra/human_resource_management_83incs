package com.hrm.app.request;

import com.hrm.app.abstraction.Designation;
import com.hrm.app.abstraction.GenderType;
import com.hrm.app.abstraction.UserType;
import com.hrm.app.entity.Document;
import com.hrm.app.entity.Education;

import javax.validation.constraints.*;
import java.util.Date;
import java.util.List;

public class UserRequest {

    private static final long serialVersionUID = 312031839686794415L;

    @NotEmpty(message = "first name cannot be blank")
    private String firstName;
    @NotEmpty(message = "last name cannot be blank")
    private String lastName;
    @Email(message = "Enter a valid Email")
    @NotEmpty(message = "email cannot not be blank")
    private String email;
    @Size(min = 1,max = 20,message = "password cannot be more than 20 characters")
    private String password;
    @Size(min = 1,max = 10,message = "Mobile no. cannot be more than 10 digits")
    private String phoneNumber;
    @PastOrPresent(message = "provide a valid date")
    private Date joiningDate;
    private UserType type;
    private Designation designation;
    private String reportingManger;
    @Past(message = "provide a valid date")
    private Date dob;
    private String addressLine1;
    private String addressLine2;
    private String martialStatus;
    private GenderType gender;
    @NotBlank
    private String bloodGroup;
    private String experince;
    private List<String> departmentsId;
    @NotBlank(message = "please provide employee role")
    private String roleId;
    private List<Education> educationList;
    private List<Document> documentList;

    public List<Document> getDocumentList() {
        return documentList;
    }

    public void setDocumentList(List<Document> documentList) {
        this.documentList = documentList;
    }

    public List<Education> getEducationList() {
        return educationList;
    }

    public void setEducationList(List<Education> educationList) {
        this.educationList = educationList;
    }



    public String getRoleId() {
        return roleId;
    }

    public void setRoleId(String roleId) {
        this.roleId = roleId;
    }

    public List<String> getDepartmentsId() {
        return departmentsId;
    }

    public void setDepartmentsId(List<String> departmentsId) {
        this.departmentsId = departmentsId;
    }

    public Date getJoiningDate() {
        return joiningDate;
    }

    public void setJoiningDate(Date joiningDate) {
        this.joiningDate = joiningDate;
    }

    public UserType getType() {
        return type;
    }

    public void setType(UserType type) {
        this.type = type;
    }

    public Designation getDesignation() {
        return designation;
    }

    public void setDesignation(Designation designation) {
        this.designation = designation;
    }

    public String getReportingManger() {
        return reportingManger;
    }

    public void setReportingManger(String reportingManger) {
        this.reportingManger = reportingManger;
    }

    public Date getDob() {
        return dob;
    }

    public void setDob(Date dob) {
        this.dob = dob;
    }

    public String getAddressLine1() {
        return addressLine1;
    }

    public void setAddressLine1(String addressLine1) {
        this.addressLine1 = addressLine1;
    }

    public String getAddressLine2() {
        return addressLine2;
    }

    public void setAddressLine2(String addressLine2) {
        this.addressLine2 = addressLine2;
    }

    public String getMartialStatus() {
        return martialStatus;
    }

    public void setMartialStatus(String martialStatus) {
        this.martialStatus = martialStatus;
    }

    public GenderType getGender() {
        return gender;
    }

    public void setGender(GenderType gender) {
        this.gender = gender;
    }

    public String getBloodGroup() {
        return bloodGroup;
    }

    public void setBloodGroup(String bloodGroup) {
        this.bloodGroup = bloodGroup;
    }


    public String getExperince() {
        return experince;
    }

    public void setExperince(String experince) {
        this.experince = experince;
    }


    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }
}
