package com.hrm.app.request;

import com.hrm.app.security.Privilege.RoleEntitlement;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.List;

public class RoleRequest {


    private String id;
    @NotNull(message = "name cannot be blank")
    private String name;
    @Size(min = 5,max = 100,message = "Description cannot be less than 5 characters")
    @NotNull(message = "provide role desicription")
    private String description;
    private List<RoleEntitlement> roleEntitlement;




    public List<RoleEntitlement> getRoleEntitlement() {
        return roleEntitlement;
    }

    public void setRoleEntitlement(List<RoleEntitlement> roleEntitlement) {
        this.roleEntitlement = roleEntitlement;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
