package com.hrm.app.request;

import com.hrm.app.abstraction.DocumentType;

public class DocumentRequest {

    private String id;
    private DocumentType type;
    private String path;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public DocumentType getType() {
        return type;
    }

    public void setType(DocumentType type) {
        this.type = type;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }
}
